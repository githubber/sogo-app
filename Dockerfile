FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/build/sope /app/code/build/sogo

RUN apt-get update && \
    apt-get install -y gobjc libgnustep-base-dev gnustep-base-common gnustep-make libldap2-dev libssl-dev libpq-dev libwbxml2-dev libmemcached-dev memcached liblzma-dev libzip-dev liboath-dev libytnef0-dev && \
    rm -rf /var/cache/apt /var/lib/apt/lists

ARG VERSION=5.9.0

WORKDIR /app/code/build/sope
RUN curl -SLf "https://github.com/Alinto/sope/archive/SOPE-${VERSION}.tar.gz" | tar -zx --strip-components 1 -f - -C /app/code/build/sope && \
    ./configure --with-gnustep && make && make install && \
    rm -rf /app/code/build/sope

WORKDIR /app/code/build/sogo
RUN curl -SLf "https://github.com/Alinto/sogo/archive/SOGo-${VERSION}.tar.gz" | tar -zx --strip-components 1 -f - -C /app/code/build/sogo && \
    ./configure --enable-mfa && make && make install && \
    rm -rf /app/code/build/sogo

RUN mkdir -p /etc/sogo && \
    rm -rf /etc/sogo/sogo.conf && ln -s /app/data/sogo.conf /etc/sogo/sogo.conf && \
    rm -rf /var/log/nginx && ln -s /run/nginx /var/log/nginx && \
    mkdir /run/GNUstep && ln -s /run/GNUstep /home/cloudron/GNUstep

# configure supervisor
RUN crudini --set /etc/supervisor/supervisord.conf supervisord logfile /run/supervisord.log && \
	crudini --set /etc/supervisor/supervisord.conf supervisord logfile_backups 0
ADD supervisor/ /etc/supervisor/conf.d/

COPY sogo.conf.template nginx.conf start.sh /app/code/

RUN chown -R cloudron:cloudron /etc/sogo
# Create the spool directory on /run and symlink the original one
RUN rm -rf /var/spool/sogo && mkdir -p /app/data/spool && ln -s /app/data/spool /var/spool/sogo

ENV LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib/sogo

WORKDIR /app/code
CMD [ "/app/code/start.sh" ]
