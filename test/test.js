#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL1 || !process.env.PASSWORD) {
    console.log('EMAIL1 and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;

    let browser, app;

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EVENT_TITLE = 'Meet the Cloudron Founders';
    const CONTACT_CN = 'Max Mustermann';
    const EMAIL = process.env.EMAIL1;
    const PASSWORD = process.env.PASSWORD;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('input_1'));
        await browser.findElement(By.id('input_1')).sendKeys(EMAIL);
        await browser.findElement(By.id('passwordField')).sendKeys(process.env.PASSWORD);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//*[text()="Inbox"]')), TEST_TIMEOUT);
    }

    async function addContact() {
        await browser.get('https://' + app.fqdn + '/SOGo/so/' + EMAIL + '/Contacts/view#/addressbooks/personal/card/new');
        await waitForElement(By.xpath('//*[@aria-label="New Contact"]'));
        await browser.findElement(By.xpath('//*[@aria-label="New Contact"]')).click();
        // open animation
        await browser.sleep(2000);
        await waitForElement(By.xpath('//*[@aria-label="Create a new address book card"]'));
        await browser.findElement(By.xpath('//*[@aria-label="Create a new address book card"]')).click();
        await waitForElement(By.xpath('//*[@ng-model="editor.card.c_cn"]'));
        await browser.findElement(By.xpath('//*[@ng-model="editor.card.c_cn"]')).sendKeys(CONTACT_CN);
        await browser.findElement(By.xpath('//*[@aria-label="Save"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//div[text()="' + CONTACT_CN + '"]')), TEST_TIMEOUT);
    }

    async function getContact() {
        await browser.get('https://' + app.fqdn + '/SOGo/so/' + EMAIL + '/Contacts/view');
        await browser.wait(until.elementLocated(By.xpath('//div[text()="' + CONTACT_CN + '"]')), TEST_TIMEOUT);
    }

    async function eventExists() {
        await browser.get('https://' + app.fqdn + '/SOGo/so/' + EMAIL + '/Calendar/view');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//*[text()[contains(., "' + EVENT_TITLE + '")]]'));
        await browser.findElement(By.xpath('//*[text()[contains(., "' + EVENT_TITLE + '")]]')).click();
        // wait for open
        await browser.sleep(2000);
        await waitForElement(By.xpath('//*[text()="' + EVENT_TITLE + '"]'));
    }

    async function visitMailbox() {
        await browser.get('https://' + app.fqdn + '/SOGo/so/' + EMAIL + '/Mail/view');
        await waitForElement(By.xpath('//*[@aria-label="Write a new message"]'));
    }

    async function visitFilters() {
        await browser.get('https://' + app.fqdn + '/SOGo/so/' + EMAIL + '/Preferences#!/mailer');
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//*[text()[contains(., "Filters")]]')).click();
        await waitForElement(By.xpath('//*[@aria-label="Create Filter"]'));
    }

    async function createEvent() {
        await browser.get('https://' + app.fqdn + '/SOGo/so/' + EMAIL + '/Calendar/view');
        await waitForElement(By.xpath('//*[@aria-label="New Event"]'));
        await browser.findElement(By.xpath('//*[@aria-label="New Event"]')).click();
        // open animation
        await browser.sleep(2000);
        await waitForElement(By.xpath('//*[@aria-label="Create a new event"]'));
        await browser.findElement(By.xpath('//*[@aria-label="Create a new event"]')).click();
        await waitForElement(By.xpath('//*[@ng-model="editor.component.summary"]'));
        await browser.findElement(By.xpath('//*[@ng-model="editor.component.summary"]')).sendKeys(EVENT_TITLE);
        await browser.findElement(By.xpath('//button[contains(text(), "Save")]')).click();
        // wait for save
        await browser.sleep(2000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);

    it('can create event', createEvent);

    it('event is present', eventExists);
    it('can add contact', addContact);
    it('can get contact', getContact);
    it('can visit mailbox', visitMailbox);
    it('can visit filters', visitFilters);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    // unlike other tests, we don't uninstall and restore because of SOGo caching old db credentials
    // https://git.cloudron.io/cloudron/sogo-app/-/issues/19
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('event is still present', eventExists);
    it('can get contact', getContact);
    it('can visit mailbox', visitMailbox);
    it('can visit filters', visitFilters);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        await browser.get('about:blank');

        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login);
    it('event is still present', eventExists);
    it('can get contact', getContact);
    it('can visit mailbox', visitMailbox);
    it('can visit filters', visitFilters);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // update tests
    it('can install app for update', function () { execSync('cloudron install --appstore-id nu.sogo.cloudronapp2 --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add contact', addContact);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can get contact', getContact);
    it('can visit filters', visitFilters);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
