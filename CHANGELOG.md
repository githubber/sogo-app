[1.0.0]
* Initial release for multi domain email

[1.1.0]
* Enable Filters UI (Sieve)

[1.1.1]
* Update SOGo to 4.0.1
* [Release announcement](https://sogo.nu/news/2018/sogo-v401-released.html)
* [web] now possible to show events/task for the current year
* [web] show current ordering setting in lists
* [web] remove invalid occurrences when modifying a recurrent event
* [web] updated Angular to version 1.7.2
* [web] updated Angular Material to version 1.1.10
* [web] updated CKEditor to version 4.10.0
* [web] allow mail flag addition/edition on mobile
* [web] added Japanese (jp) translation - thanks to Ryo Yamamoto

[1.1.2]
* Update SOGo to 4.0.2
* [Release announcement](https://sogo.nu/news/2018/sogo-v402-released.html)
* [web] prohibit duplicate calendar categories in Preferences module
* [web] added Romanian (ro) translation - thanks to Vasile Razvan Luca
* [web] add security flags to cookies (HttpOnly, secure) (#4525)
* [web] better theming for better customization (#4500)
* [web] updated Angular to version 1.7.3
* [web] updated ui-router to version 1.0.20

[1.2.0]
* Update to new Cloudron base image

[1.2.1]
* Update SOGo to 4.0.3
* [Release announcement](https://sogo.nu/news/2018/sogo-v403-released.html)
* [web] moved the unseen messages count to the beginning of the window’s title (#4553)
* [web] allow export of calendars subscriptions (#4560)
* [web] hide compose button when reading message on mobile device
* [web] include mail account name in form validation (#4532)
* [web] calendar properties were not completely reset on cancel
* [web] check ACLs on address book prior to delete cards
* [web] fixed condition of copy action on cards

[1.2.2]
* Update SOGo to 4.0.4

[1.2.3]
* Update usage information

[1.2.4]
* Update SOGo to 4.0.5

[1.3.0]
* Enable vacation mail and forwarding by default

[1.3.1]
* Update SOGo to 4.0.6

[1.3.2]
* Update SOGo to 4.0.7

[1.4.0]
* Update to Cloudron manifest v2

[1.4.1]
* Update SOGo to 4.0.8
* If login fails 5 times in 10 seconds, block for 300 seconds

[2.0.0]
* Fix addressbook and calendar sharing
* Setup email aliases configured for mailboxes

[2.0.1]
* Set junk folder name

[2.1.0]
* Use latest base image 2.0.0
* Update SOGo to 4.3.1

[2.1.1]
* Update SOGo to 4.3.2

[2.2.0]
* Update SOGo to 5.0.0
* [Release announcement](https://sogo.nu/news/2020/sogo-v500-released.html)
* preferences: button to reset contacts categories to defaults (76cbe78)
* web: support desktop notifications, add global inbox polling (87cf5b4 8205acc), closes #1234 #3382 #4295

[2.2.1]
* Update SOGo to 5.0.1
* Enable 2FA

[2.3.0]
* Update base image to v3

[2.4.0]
* Update SOGo to 5.1.0
* [Release announcement](https://www.sogo.nu/news/2021/sogo-v510-released.html)
* calendar(js): allow HTML links in location field (0509d7f)
* calendar(web): allow to change the classification of an event (4a83733)
* mail: new option to force default identity (fc4f5d2)
* calendar: accept HTML in repeat frequencies descriptions (c38524a)
* calendar: avoid exception when FoldersOrder have invalid entries (c27be0f)
* calendar: fix all-day events in lists (5d1ac9d)

[2.4.1]
* Update SOGo to 5.1.1
* [Release announcement](https://www.sogo.nu/news/2021/sogo-v511-released.html)
* addressbook: import contact lists from LDIF file (e1d8d70), closes #3260
* calendar(css): enlarge categories color stripes (bd80b6e), closes #5301
* calendar(css): enlarge categories color stripes (e5d9571), closes #5301
* calendar(js): fix URL for snoozing alarms (d4a0b25), closes #5324
* calendar(js): show conflict error inside appointment editor (fec299f)
* core: avoid appending an empty domain to uid in cache (debcbd1)
* core: change password in user's matching source only (da36608)

[2.4.2]
* Make sogo.conf customizable
* Disable Gravatar by default

[2.5.0]
* Update SOGo to 5.2.0
* [Release announcement](https://www.sogo.nu/news/2021/sogo-v520-released.html)
* mail: download message as .eml file (ef5e777)
* mail: initial support for ms-tnef (winmail.dat) body part (045f134), closes #2242 #4503
* mail: new parameter to disable S/MIME certificates (545cfe5)

[2.6.0]
* Update SOGo to 5.3.0
* [Release announcement](https://www.sogo.nu/news/2021/sogo-v530-released.html)
* addressbook: warn when similar contacts are found (a14c456)
* mail: add support for UID MOVE operation (d1fc15b)
* mail: allow to directly empty junk folder (6c56340), closes #5224
* mail: filter mailbox by flagged messages (c2f95dc), closes #1417

[2.7.0]
* Use the internal non-TLS imap port.

[2.8.0]
* Update SOGo to 5.4.0
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.4.0)

[2.9.0]
* Update SOGo to 5.5.0
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.5.0)
* eas: implement replyTo element

[2.9.1]
* Update SOGo to 5.5.1
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.5.1)
* addressbook(dav): return all value of all properties (940394d), closes #5462
* addressbook: close DB connection after DAV query (348a79c)
* calendar(dav): add DAV:status to DAV:response only when deleted (b061046), closes #5163
* calendar: display start & end dates in mail notifications (663915b)
* calendar: handle DST change for all-day events (0c5a269)

[2.10.0]
* Update SOGo to 5.6.0
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.6.0)
* calendar: optionally remove attendees that can't be invited (048858b, 4e5c865)
* calendar: search for past events and tasks (2f725a3), closes #5441
* mail(web): forward multiple messages as attachments (c476503), closes #33
* mail(web): Templates folder for pre-defined drafts (21dc73d, c986422), closes #4320 #5363
* mail: always forward incoming messages, before all filters (8ed4fc8)

[2.10.1]
* Fix session expiry issue

[2.11.0]
* Update SOGo to 5.7.0
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.7.0)
* addressbook(web): import vList from versit file (51dc929)
* preferences: password constraints for SQL sources (2ef849c)
* calendar: disable wrapping of labels in mail templates (bd40b95)
* calendar: filter by matching property values (5452cd7)

[2.11.1]
* Update SOGo to 5.7.1
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.7.1)
* addressbook(dav): improve handling of addressbook-query (0720dc4)
* calendar: update all quick fields when updating event (a6f7c1e), closes #5546
* calendar: remove time from until date of rrule of all-day event (3f7fef0)
* calendar(js): conditional display of edit and delete buttons (16459c7)
* core: don’t remove double quotes when parsing versit strings (6bed6c0), closes #3530 #3930
* core: don’t remove double quotes when parsing versit strings (ebf032e d99aa82), closes #3530
* core: handle argon2i password scheme (2f9f610), closes #5531
* dav: return 405 when collection already exists (95eecc4), closes /datatracker.ietf.org/doc/html/rfc4918#section-9
* i18n: new localized strings for SQL-based password policies (5e6ad77)
* mail(js): add CKEditor plugins emoji and bidi (e18dc2c)
* mail(js): handle paste action from contextual menu (efe78c5), closes #5488
* mail(js): resolve sub mailboxes in global search (407bc53), closes #5559
* mail: use encoding from MIME part (27d45b6)
* preferences(js): don’t filter vacation start date (2ae03d5)
* preferences: regex in Sieve scripts are now case sensitive (adfd175), closes #5561
* web(js): improve validation of email addresses (67ccf74)

[2.12.0]
* Update SOGo to 5.8.0
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.8.0)
* password-recovery: Add password recovery with secret question or secondary email
* mail: Improve IMAP fetch sorting using NSDictionary keys instead of indexOfObject (40b5c09 48c7375 60ec315 38e886a)
* calendar: Add SOGoDisableOrganizerEventCheck parameter - this parameter is used to avoid checking calendar event's organizer (cddfdb9)
* calendar: Refresh data when clicking on 'today' (5fb82fe)
* login: Add button to discover password (7bfa900)

[2.12.1]
* Update SOGo to 5.8.1
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.8.1)
* alarms: Add possibility to use SMTP master account for system alarms. Fixes #5565. (53f9449 7faf4eb 8001e3f)
* core: Add SOGoDisableSharing option to disable sharing for mail, contacts and calendar (435495f)
* eas: Add SOGoEASDisableUI system parameter to remove EAS options in UI when ActiveSync is not used (04e15b5)
* mail: Add punycode (RFC 3492) to encode domain with special characters (e12583e 0a9b3d6 39032ac 13178a1)
* preferences: Improve TOTP - add validation code in preferences before saving to ensure user add qr code in totp application (0bd530a)

[2.12.2]
* Update SOGo to 5.8.2
* [Full changelog](https://github.com/inverse-inc/sogo/releases/tag/SOGo-5.8.2)
* calendar: Impossible to create a new event after creating a new one. Closes #5718 (f813b78c)

[2.12.3]
* Update SOGo to 5.8.3
* [Full changelog](https://github.com/Alinto/sogo/releases/tag/SOGo-5.8.3)
* calendar: Add week number on month view. Fixes #4457. (96639b7)
* mail: Add autolink from ckeditor. Fixes #5749 (810cd12)
* preferences: Add SOGoCreateIdentitiesDisabled domain option which disables identity creation for users in preferences (af6202b a14f9a7)

[2.12.4]
* Update SOGo to 5.8.4
* [Full changelog](https://github.com/Alinto/sogo/releases/tag/SOGo-5.8.4)
* preferences: Fix NSException issue when SOGoCreateIdentitiesDisabled is set (ef958f6)

[2.12.5]
* Default to showing only subscribed folders like other mail clients

[2.13.0]
* Update SOGo to 5.9.0
* [Full changelog](https://github.com/Alinto/sogo/releases/tag/SOGo-5.9.0)
* This is a major release of SOGo which add new features and a lots of bug fixes.

